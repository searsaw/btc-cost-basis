# BTC Cost Basis CLI (bcb)

This simple CLI takes a set of files from different sources, parses them, and runs a simple calculation to determine the average cost basis for the BTC purchases in the files. Currently, the following sources are supported:

- [Swan Bitcoin History CSV](https://swanbitcoin.com)
- [Gemini Exchange Transaction History Excel File](https://exchange.gemini.com)

## Install

Visit [the releases page](https://gitlab.com/searsaw/btc-cost-basis/-/releases) to download the most recent version of the CLI.

## Usage

```shell
bcb [--method method] [--swan path/to/file]... [--gemini path/to/file]...
  -gemini value
    	a list of Excel files from Gemini
  -method string
    	the method of calculating the cost basis (acb) (default "acb")
  -swan value
    	a list of CSVs from Swan Bitcoin
  -version
    	display the CLI version
```

To get the cost basis for data from a Swan CSV and two Gemini history files:

```shell
bcb --swan swan_history.csv --gemini gemini_history1.xlsx --gemini gemini_history2.xlsx
```

## Development

Clone down the source and use `go` to run it.

```shell
git clone git@gitlab.com:searsaw/btc-cost-basis.git
cd btc-cost-basis
go run ./cmd/bcb --swan swan_history.csv
```
