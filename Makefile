export CGO_ENABLED=0

VERSION=$(shell git describe --always)
LDFLAGS=-ldflags "-w -s -X main.Version=${VERSION}"

build: build-linux build-darwin

.PHONY: build-darwin
build-darwin:
	@GOOS=darwin GOARCH=amd64 go build -o bcb_darwin_amd64 ${LDFLAGS} ./cmd/bcb

.PHONY: build-linux
build-linux:
	@GOOS=linux GOARCH=amd64 go build -o bcb_linux_amd64 ${LDFLAGS} ./cmd/bcb

.PHONY: vet
vet:
	@go vet ./...

.PHONY: fmt
fmt:
	@gofmt -d -e -l -s .

.PHONY: clean
clean:
	@rm bcb_*
