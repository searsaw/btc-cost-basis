package data

import (
	"fmt"
	"io"
	"time"
)

// Parser is a function that takes a writer and parses
// the given file into a slice of Data
type Parser func(io.Reader) ([]Data, error)

// Source is the site or location the data is coming from
type Source string

var (
	// SourceSwan represents data from Swan Bitcoin
	SourceSwan Source = "Swan Bitcoin"
	// SourceGemini represents data from Gemini
	SourceGemini Source = "Gemini"
)

// ErrUnknownSource represents an unknown source
type ErrUnknownSource struct {
	Source string
}

func (e ErrUnknownSource) Error() string {
	return fmt.Sprintf("unknown Source %s", e.Source)
}

// Kind is the type of data, i.e. purchase, sell
type Kind string

var (
	// KindPurchase represents a purchase of Bitcoin
	KindPurchase Kind = "purchase"
)

// Data represents a single piece of data about a BTC purchase,
// such as a single line in a CSV for a buy
type Data struct {
	// The location the data came from
	Source Source
	// The type of data point
	Kind Kind
	// The date and time of the purchase
	Date time.Time
	// The amount paid to purchase the BTC, in USD cents
	AmountPaid int64
	// The amount of BTC purchased, in Satoshis
	AmountPurchased int64
	// The price of BTC at time of purchase, in USD cents
	BitcoinPrice int64
}
