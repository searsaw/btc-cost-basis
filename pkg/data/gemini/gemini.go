package gemini

import (
	"fmt"
	"io"
	"math"
	"strconv"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"gitlab.com/searsaw/btc-cost-basis/pkg/data"
)

const (
	SheetNameHistory = "Account History"
)

func Parse(r io.Reader) ([]data.Data, error) {
	excel, err := excelize.OpenReader(r)
	if err != nil {
		return nil, fmt.Errorf("error reading excel file: %w", err)
	}

	rows, err := excel.GetRows(SheetNameHistory)
	if err != nil {
		return nil, fmt.Errorf("error getting the rows of the sheet: %w", err)
	}

	var d []data.Data

	// skip the header row
	for _, row := range rows[1:] {
		if row[2] != "Buy" {
			continue
		}

		combinedDate := row[0] + " " + row[1]
		buyDate, err := time.Parse("2006-01-02 15:04:05", combinedDate)
		if err != nil {
			return nil, fmt.Errorf("error parsing Excel row date '%s': %w", combinedDate, err)
		}

		amountPaid, err := strconv.ParseFloat(row[7], 64)
		if err != nil {
			return nil, fmt.Errorf("error parsing Excel row amount paid '%s': %w", row[7], err)
		}

		feePaid, err := strconv.ParseFloat(row[8], 64)
		if err != nil {
			return nil, fmt.Errorf("error parsing Excel fee paid '%s': %w", row[8], err)
		}

		amountPurchased, err := strconv.ParseFloat(row[10], 64)
		if err != nil {
			return nil, fmt.Errorf("error parsing Excel row amount purchased '%s': %w", row[10], err)
		}

		totalPaid := math.Abs(amountPaid + feePaid)

		d = append(d, data.Data{
			Source:          data.SourceGemini,
			Kind:            data.KindPurchase,
			Date:            buyDate,
			AmountPaid:      int64(totalPaid * 100),
			AmountPurchased: int64(amountPurchased * 100_000_000),
			BitcoinPrice:    0,
		})
	}

	return d, nil
}
