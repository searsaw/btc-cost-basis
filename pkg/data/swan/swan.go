package swan

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"gitlab.com/searsaw/btc-cost-basis/pkg/data"
)

func Parse(r io.Reader) ([]data.Data, error) {
	var d []data.Data
	csvFile := csv.NewReader(r)

	for {
		row, err := csvFile.Read()
		if err == io.EOF {
			return d, nil
		}
		if err != nil {
			return nil, fmt.Errorf("error getting Swan data: %w", err)
		}

		// Swan doesn't offer sells
		if row[0] != "purchase" {
			continue
		}

		buyDate, err := time.Parse("2006-01-02 15:04:05+00", row[1])
		if err != nil {
			return nil, fmt.Errorf("error parsing CSV row date '%s': %w", row[1], err)
		}

		amountPaid, err := strconv.ParseFloat(row[3], 64)
		if err != nil {
			return nil, fmt.Errorf("error parsing CSV row amount paid '%s': %w", row[3], err)
		}

		amountPurchased, err := strconv.ParseFloat(row[4], 64)
		if err != nil {
			return nil, fmt.Errorf("error parsing CSV row amount purchased '%s': %w", row[4], err)
		}

		btcPrice, err := strconv.ParseFloat(row[5], 64)
		if err != nil {
			return nil, fmt.Errorf("error parsing CSV row Bitcoin price '%s': %w", row[5], err)
		}

		d = append(d, data.Data{
			Source:          data.SourceSwan,
			Kind:            data.KindPurchase,
			Date:            buyDate,
			AmountPaid:      int64(amountPaid * 100),
			AmountPurchased: int64(amountPurchased * 100_000_000),
			BitcoinPrice:    int64(btcPrice * 100),
		})
	}
}
